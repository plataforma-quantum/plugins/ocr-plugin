<?php

return [
    'name' => 'OCR',

    /**
     * Plugin services
     *
     */
    'services' => [
        'document'      => \Plugins\OCR\Services\DocumentService::class,
        'google_vision' => \Plugins\OCR\Services\GoogleVisionService::class,
    ]
];
