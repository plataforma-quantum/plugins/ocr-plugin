<?php

namespace Plugins\OCR\Models;

class DUT extends Document
{
    /**
     * Get license plate
     *
     */
    public function getLicensePlate()
    {
        return $this->getPartial('UF PLACA ', 7);
    }

    /**
     * Gets the document number
     *
     */
    public function getDocumentNumber()
    {
        return $this->getPartial('No ', 12);
    }

    /**
     * Get owner name
     *
     */
    public function getOwnerName()
    {
        return $this->getPartial('Nome ', 12);
    }
}
