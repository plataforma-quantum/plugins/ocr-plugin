<?php

namespace Plugins\OCR\Models;

class Document
{
    /**
     * Source string from document
     *
     */
    public $source;

    const CPF_REGEX = '/([0-9]{2}[\.]?[0-9]{3}[\.]?[0-9]{3}[\/]?[0-9]{4}[-]?[0-9]{2})|([0-9]{3}[\.]?[0-9]{3}[\.]?[0-9]{3}[-]?[0-9]{2})/';

    /**
     * Constructor method
     *
     */
    public function __construct(string $source)
    {
        $this->source = $source;
    }

    public function sanitizeMatches($matches)
    {
        return array_unique(collect($matches)->filter(function ($item) {
            return !empty($item);
        })->toArray());
    }

    /**
     * Get cpf from string
     *
     */
    public function getCpf()
    {
        $matches = [];
        if (!preg_match(self::CPF_REGEX, $this->source, $matches)) {
            return null;
        }
        return $this->sanitizeMatches($matches)[0];
    }

    /**
     * Get partial text
     *
     */
    public function getPartial(string $start, $lenght)
    {
        $index = strpos($this->source, $start) + strlen($start);
        return substr($this->source, $index, $lenght);
    }

    public function getDocumentNumbers()
    {
        $matches = [];
        preg_match_all('!\d+!', $this->source, $matches);
        return $matches;
    }

    public function getDocumentDates()
    {
    }

    public function getDocumentTexts()
    {
    }
}
