<?php

namespace Plugins\OCR\Services;

use Plugins\OCR\Models\Document;

class DocumentService
{

    /**
     * File path
     *
     */
    public $filePath;

    /**
     * Document instance
     *
     */
    public $document;

    /**
     * Open document
     *
     */
    public function open(string $filePath, $type)
    {
        $data = _q('ocr')->service('google_vision')->open($filePath)->run();
        $this->document = new $type($data);
        return $this->document;
    }
}
