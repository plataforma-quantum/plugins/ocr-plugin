<?php

namespace Plugins\OCR\Services;

use Google\Cloud\Vision\V1\ImageAnnotatorClient;
use Google\Cloud\Vision\V1\Feature\Type;

class GoogleVisionService
{

    public $filePath;

    public $fileContent;

    public $imageAnnotator;

    public function __construct(ImageAnnotatorClient $imageAnnotator)
    {
        $this->imageAnnotator = $imageAnnotator;
    }

    /**
     * Open a file into memory
     *
     */
    public function open(string $filePath)
    {
        $this->fileContent = file_get_contents($filePath);
        $this->filePath = $filePath;
        return $this;
    }

    /**
     * Execute document read
     *
     */
    public function run()
    {
        $response = $this->imageAnnotator->annotateImage($this->fileContent, [Type::TEXT_DETECTION]);
        $response->discardUnknownFields();
        $data = json_decode($response->serializeToJsonString(), TRUE);
        return str_replace("\n", " ", $data['fullTextAnnotation']['text']);
    }
}
