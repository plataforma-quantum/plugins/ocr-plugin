<?php

namespace Plugins\OCR\Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Plugins\OCR\Models\DUT;

class GoogleVisionServiceTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testExample()
    {
        $googleService = _q('ocr')->service('document');

        $cnhPath = __DIR__ . '/../Assets/dut_test.jpeg';

        $document = $googleService->open($cnhPath, DUT::class);

        dd($document->getOwnerName(), $document->getDocumentNumber(), $document->source);
        dd($document->getLicensePlate(), $document->source);

        $this->assertTrue(true);
    }
}
